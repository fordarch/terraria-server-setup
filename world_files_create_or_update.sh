#!/bin/bash


# !!ASSUMPTIONS!!
# The server instance is not running.
# There is no Terraria instance (world) running that relys on the files targeted by 
#   this script. This script will delete the files of current server server instance
#   targeted by this script (if any).
#   If you already started a server instance with this exact script, stop the instance or  
#   you will likely experience weird, unwanted, behavior. 
#
# User executing these commands will be the Terraria administrator. 
#  Consider creating a user:terraria to manage all things Terraria.
#
# Environment is Ubuntu Linux headless server x64


#########################################
# USAGE
#########################################
# $./source_files_get.sh -w <world name> [-y]
#   -y, ACCEPT DEFAULT VALUES
#       Setting this flag confirms the user is ready to accept the configuration 
#       settings in this file. 

#########################################
# CONFIG
#########################################
# ---------------------------------------------------------------
# Keep these value up-to-date to match the current SERVER version number
# ---------------------------------------------------------------
VERSION=1449
TERRARIA_URL='https://terraria.org/api/download/pc-dedicated-server/terraria-server-1449.zip'

# This is the Linux user name of the owner of the terraria files.
# This user must exist. Files may be created and deleted in this user's home directory. 
TERRARIA_ADMIN_USER_NAME='terraria'    

# ---------------------------------------------------------------
# Check command-line arguments
# ---------------------------------------------------------------
flag_y=false

CURRENT_WORLD_NAME=""

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -y|--yass)
            flag_y=true
            ;;
        -h|--help)
            echo "Usage: $0 [-y]"
            ;;
        -w|--world)
            CURRENT_WORLD_NAME="$2"
            shift
            ;;
        # Add other flags as needed
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
    shift
done

if [ -z "$CURRENT_WORLD_NAME" ]; then
    echo "The current world name [-w|--world] was not provided. Unable to continue."
    exit 1
fi


# ---------------------------------------------------------------
# Set the following variables with your desired paths or 
#   leave them with the default values if it suits you.
# Default values assume you will adopt a one directory per terraria:world policy
# e.g. 
# terraria
# └── worlds/
#     ├── world_1/
#     │   └──  srv/
#     │   └──  staging/
#     │   └──  latest/
#     │   └──  archive/
#     └── world_2/
#     │   └──  srv/
#     │   └──  staging/
#     │   └──  latest/
#     │   └──  archive/
#     ...
# ---------------------------------------------------------------


# You can probably leave these values alone unless you want to custom tailor your installation.
TERRARIA_HOME=/home/$TERRARIA_ADMIN_USER_NAME
WORLDS_HOME=$TERRARIA_HOME/worlds
TARGET_WORLD_HOME=$WORLDS_HOME/$CURRENT_WORLD_NAME
TERRARIA_SRV=$TARGET_WORLD_HOME/srv
TERRARIA_STAGE=$TARGET_WORLD_HOME/staging
TERRARIA_LATEST=$TERRARIA_SRV/latest
TERRARIA_ARCHIVE=$TERRARIA_SRV/archive

WORLD_SERVER_CONFIG=$TARGET_WORLD_HOME/serverconfig.txt
SCREEN_NAME=terraria_$CURRENT_WORLD_NAME

# --------------------------------------
# Style
# --------------------------------------
RED='\033[0;31m'
GREEN='\033[0;32m'
LTCYAN='\033[1;36m'
NC='\033[0m' # No Color

#########################################
# SETUP
#########################################

# Check admin user for existance

if ! id "$TERRARIA_ADMIN_USER_NAME" &>/dev/null; then
    echo -e "${RED}ERROR${NC} Provided user name $TERRARIA_ADMIN_USER_NAME is not a known user. Unable to proceed."
    exit 1
fi

# Check provided world name is valid

if [[ ! $CURRENT_WORLD_NAME =~ ^[a-zA-Z]+[a-zA-Z0-9_]*$ ]]; then 
    echo -e "${RED}ERROR${NC} World name $CURRENT_WORLD_NAME contains invalid characters. Use letters, numbers, and underscores only. First character should be a letter."
    exit 1
fi

# Drop any SCREEN sessions that may be running with a world-name matching that which was provided to this script. 
if [ "$flag_y" = true ]; then
    screen -ls | awk -v target="$SCREEN_NAME" '$0 ~ target {print $1}' | xargs -I{} screen -S {} -X quit
fi

# ---------------------------------------------------------------
# Check for presence of required directories
#   as specified in above configuration section
# ---------------------------------------------------------------
if [ "$flag_y" = false ]; then
    # User has not specified the -y flag, suggesting the required directories exist.
    # Here we check if the user made a mistake.

    [ -d $TERRARIA_HOME ] || { echo "Error: Directory $TERRARIA_HOME does not exist"; exit 1; }
    [ -d $WORLDS_HOME ] || { echo "Error: Directory $WORLDS_HOME does not exist"; exit 1; }
    [ -d $TARGET_WORLD_HOME ] || { echo "Error: Directory $TARGET_WORLD_HOME does not exist"; exit 1; }
    [ -d $TERRARIA_SRV ] || { echo "Error: Directory $TERRARIA_SRV does not exist"; exit 1; }
    [ -d $TERRARIA_STAGE ] || { echo "Error: Directory $TERRARIA_STAGE does not exist"; exit 1; }
    [ -d $TERRARIA_LATEST ] || { echo "Error: Directory $TERRARIA_LATEST does not exist"; exit 1; }
    [ -d $TERRARIA_ARCHIVE ] || { echo "Error: Directory $TERRARIA_ARCHIVE does not exist"; exit 1; }

else
    # Here the user HAS specified the -y flag, suggesting this MAY be the first time the script has ran 
    # We check for the existance for required directories, but with the provided -y flag 
    # we assume the user's permission to create directories if they do not exist.
    
    [[ ! -d $TERRARIA_HOME ]] && mkdir $TERRARIA_HOME
    [[ ! -d $WORLDS_HOME ]] && mkdir $WORLDS_HOME
    [[ ! -d $TARGET_WORLD_HOME ]] && mkdir $TARGET_WORLD_HOME
    [[ ! -d $TERRARIA_SRV ]] && mkdir $TERRARIA_SRV
    [[ ! -d $TERRARIA_STAGE ]] && mkdir $TERRARIA_STAGE
    [[ ! -d $TERRARIA_LATEST ]] && mkdir $TERRARIA_LATEST
    [[ ! -d $TERRARIA_ARCHIVE ]] && mkdir $TERRARIA_ARCHIVE
fi

# ------------------------------------------
# Check directories for correct permissions.
# ------------------------------------------
if [[ ! -r $TERRARIA_HOME || ! -w $TERRARIA_HOME || ! -x $TERRARIA_HOME ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TERRARIA_HOME}"
    exit 1
fi

if [[ ! -r $WORLDS_HOME || ! -w $WORLDS_HOME || ! -x $WORLDS_HOME ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$WORLDS_HOME}"
    exit 1
fi

if [[ ! -r $TARGET_WORLD_HOME || ! -w $TARGET_WORLD_HOME || ! -x $TARGET_WORLD_HOME ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TARGET_WORLD_HOME}"
    exit 1
fi

if [[ ! -r $TERRARIA_SRV || ! -w $TERRARIA_SRV || ! -x $TERRARIA_SRV ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TERRARIA_SRV}"
    exit 1
fi

if [[ ! -r $TERRARIA_STAGE || ! -w $TERRARIA_STAGE || ! -x $TERRARIA_STAGE ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TERRARIA_STAGE}"
    exit 1
fi

if [[ ! -r $TERRARIA_LATEST || ! -w $TERRARIA_LATEST || ! -x $TERRARIA_LATEST ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TERRARIA_LATEST}"
    exit 1
fi

if [[ ! -r $TERRARIA_ARCHIVE || ! -w $TERRARIA_ARCHIVE || ! -x $TERRARIA_ARCHIVE ]]
then
    echo -e "${RED}ERROR${NC}: Permissions (rwx) missing for {$TERRARIA_ARCHIVE}"
    exit 1
fi



#########################################
# STAGING
#########################################

# Clean the stage

echo -e "${LTCYAN}INFO${NC}: Removing any old files from the staging directory"
find $TERRARIA_STAGE -mindepth 1 -delete

# Get latest server from web source

echo -e "${LTCYAN}INFO${NC}: Extracting new server zip file from web"

# Web location subject to change w/o notice
# This is kept up-to-date by the user in the configuration defined earlier in this script 

wget $TERRARIA_URL --directory-prefix=$TERRARIA_STAGE

if [[ `ls $TERRARIA_STAGE | wc -l` != 1 ]]
then
    echo -e "${RED}ERROR${NC}: Exactly 1 file expected staged after pulling down new server and this is not the case"
    exit 1
fi

if [[ ! `unzip $TERRARIA_STAGE/terraria-server-* -d $TERRARIA_STAGE` ]]
then
    echo -e "${RED}ERROR${NC}: Failed to unzip server"
    exit 1
fi

# We downloaded and unzipped a file and expect a certain directory structure to exist
# We look before we leap, but presume the directory structure remains the same. 

if [ -f $TERRARIA_STAGE/$VERSION/Linux/TerrariaServer.bin.x86_64 ]
then
    chmod +x $TERRARIA_STAGE/$VERSION/Linux/TerrariaServer.bin.x86_64
else
    echo -e "${RED}ERROR${NC}: Directory structure of Terraria package is unexpected"
    exit 1
fi

echo -e "${LTCYAN}INFO${NC}: Staging complete"
echo -e "${LTCYAN}INFO${NC}: Begin archive of previous server files"


#########################################
# ARCHIVING
#########################################

# If any previous existing files are found, create an archive of them.

TIMESTAMP=`date +%s`
TERRARIA_ARCHIVE_FILE=$TIMESTAMP.tar.gz

if [[ `ls $TERRARIA_LATEST | wc -l` -gt 0 ]]
then
    cp -r $TERRARIA_LATEST /tmp/$TIMESTAMP
    [[ `tar czf $TERRARIA_ARCHIVE/$TERRARIA_ARCHIVE_FILE /tmp/$TIMESTAMP` ]] && rm -r /tmp/$TIMESTAMP

    # Check that an archive file was made.

    if [[ -s $TERRARIA_ARCHIVE/$TERRARIA_ARCHIVE_FILE && -f $TERRARIA_ARCHIVE/$TERRARIA_ARCHIVE_FILE ]]
    then
        echo -e "${LTCYAN}INFO${NC}: Previous server directory archived."
        echo -e "`stat -c "%y    %n:    %s(bytes)" $TERRARIA_ARCHIVE/$TERRARIA_ARCHIVE_FILE`"
    else
        echo -e "${RED}ERROR${NC}: Archive file is empty or does not exist"
        exit 1
    fi
fi

# Archive rotation

if [[ `ls $TERRARIA_ARCHIVE | wc -l` -gt 2 ]]
then
    rm `ls -dt $TERRARIA_ARCHIVE/* | awk 'NR>2'`
fi

#########################################
# UPDATE
#########################################

# The current server files, if existing, have been archived.
# We need to delete the current server files and replace with the files that 
# have just been downloaded and extracted to the archive directory.

# There will be problems if the current server files are still executing.
# We check that the user has empowered us with permission.
if [ "$flag_y" = false ]; then
    # The user has NOT permitted us access to delete the current server files.
    echo -e "${LTCYAN}INFO${NC}: Elevated permission flag not set [-y]. Permission has not been granted to delete the current server files at $TERRARIA_LATEST and replace the contents with the new files in $TERRARIA_STAGE."
    exit 0
fi

echo -e "${LTCYAN}INFO${NC}: Removing all files inside $TERRARIA_LATEST"

# Delete any old files in TERRARIA_LATEST, if there are any

if [[ `ls $TERRARIA_LATEST | wc -l` -gt 0 ]]
then
    find $TERRARIA_LATEST -mindepth 1 -delete

    if [[ `ls $TERRARIA_LATEST | wc -l` -gt 0 ]]
    then
        echo -e "${RED}ERROR${NC}: Failed to delete old files from $TERRARIA_LATEST. Please investigate issue and re-run script."
        exit 1
    fi
fi

# Move our new staged files to their rightful place in TERRARIA_LATEST

mv $TERRARIA_STAGE/$VERSION/Linux/* $TERRARIA_LATEST

if [[ `ls $TERRARIA_STAGE/$VERSION/Linux/ | wc -l` != 0 || `ls $TERRARIA_LATEST | wc -l` == 0 ]]
then
    echo -e "${RED}ERROR${NC}: Migrating files from $TERRARIA_STAGE/$VERSION/Linux to $TERRARIA_LATEST failed."
    exit 1
fi

# Cleanup

echo -e "${LTCYAN}INFO${NC}: Removing old files from the staging directory"
find $TERRARIA_STAGE -mindepth 1 -delete

if [[ `ls $TERRARIA_STAGE | wc -l` -gt 0 ]]
then
    echo -e "${RED}ERROR${NC}: Failed to delete old files from $TERRARIA_STAGE. Please investigate issue and re-run script."
    exit 1
fi



# Write this default configuration to file.

CURRENT_WORLD_CONFIG="world=$TARGET_WORLD_HOME/$CURRENT_WORLD_NAME.wld
autocreate=3
worldname=$CURRENT_WORLD_NAME
difficulty=2
maxplayers=8
port=7777
worldpath=$TARGET_WORLD_HOME
language=en-US
npcstream=0
priority=1
"

echo "$CURRENT_WORLD_CONFIG" > $WORLD_SERVER_CONFIG
echo "A default configuration for the world was created at $WORLD_SERVER_CONFIG"


# Script to start the server using SCREEN

SERVER_START_COMMAND="#!/bin/bash -e

target_port=7777
is_port_in_use() {
    netstat -an | grep -E \":\$1\s\" > /dev/null
}
while is_port_in_use \"\$target_port\"; do
    ((target_port++))
done

source \"$WORLD_SERVER_CONFIG\"
new_port=\$target_port

echo \"world=\$world\" > \"$WORLD_SERVER_CONFIG\"
echo \"autocreate=\$autocreate\" >> \"$WORLD_SERVER_CONFIG\"
echo \"worldname=\$worldname\" >> \"$WORLD_SERVER_CONFIG\"
echo \"difficulty=\$difficulty\" >> \"$WORLD_SERVER_CONFIG\"
echo \"maxplayers=\$maxplayers\" >> \"$WORLD_SERVER_CONFIG\"
echo \"port=\$target_port\" >> \"$WORLD_SERVER_CONFIG\"
echo \"worldpath=\$worldpath\" >> \"$WORLD_SERVER_CONFIG\"
echo \"language=\$language\" >> \"$WORLD_SERVER_CONFIG\"
echo \"npcstream=\$npcstream\" >> \"$WORLD_SERVER_CONFIG\"
echo \"priority=\$priority\" >> \"$WORLD_SERVER_CONFIG\"

/usr/bin/screen -dmS $SCREEN_NAME /bin/bash -c \"$TERRARIA_LATEST/TerrariaServer.bin.x86_64 -config $WORLD_SERVER_CONFIG\""

echo "$SERVER_START_COMMAND" > $TARGET_WORLD_HOME/start_world.sh
chmod +x $TARGET_WORLD_HOME/start_world.sh
echo "A script to start world $CURRENT_WORLD_NAME was created at $TARGET_WORLD_HOME/start_world.sh"





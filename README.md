# Terraria Server Setup 
This project is a collection of scripts that each play a role in setting up a new headless Ubuntu server for use as a host for Terraria.


## Some assumptions
* Ubuntu Server 22.04.4 LTS (Jammy)
* Application Netstat is available (apt install net-tools)


## How stuff works
* Working with SCREEN. 
    * Each world is launched into a new SCREEN session
    * screen -ls                        # Look at the screen sessions and find each session ID
    * screen -S <session_id> -X quit    # Kill a session by ID (Don't forget to save your world.)
* Working with netstat  
    * Helpful to identify if your world is running and what port it is using.
    * netstat -tulpn        # List applications and the ports they use


## Usage
Run the setup script.  
Replace `MyCoolWorld` with whatever name you want for your world.  
Only numbers, letters (upper and lower), and underscores are allowed. First character must be a letter.
`./world_files_create_or_update.sh -w MyCoolWorld -y`

The script downloads and sets up the latest server files.  
It also creates two handy files.

; serverconfig.txt
This is a default configuration for a Terraria server. Modify it as you want or run it as-is.
`./~/worlds/MyCoolWorld/serverconfig.txt`

; start_world.sh
This will launch the server for your target world (MyCoolWorld in this example).  
The configuration in `serverconfig.txt` is read and used.  
If the port is already taken, the next highest available port number will be used. 
If the port used is different that the port in the configuration file, the configuration file will be updated with the new port number for reference. 
`./~/worlds/MyCoolWorld/start_world.sh`

Each time the script is ran for a world it will either create a new world, if the world did not previously exist;  
or, if the script was given permission [-y] it will archive the current files, download and extract the latest server files, 
set permissions, and cleanup/delete old and unnecessary files. Each world will get its own directory. See picture of the tree for how this looks.  
![Tree](tree.png)


## Configuration  
The following two variable in the script must be kept up-to-date.
```
VERSION=1449
TERRARIA_URL='https://terraria.org/api/download/pc-dedicated-server/terraria-server-1449.zip'
```

#!/bin/bash

# --------------- USAGE ----------------------
# ./terraria_user_account.sh <name of user>
# ./terraria_user_account.sh terraria
# --------------------------------------------


# Get the username as a command-line argument
username=$1

# Check if the username is provided
if [ -z "$username" ]; then
    echo "Usage: $0 <username>"
    exit 1
fi

# Check if the user already exists
if id "$username" &>/dev/null; then
    echo "User $username already exists."
else
    # Get a password for the new user
    read -s -p "Enter password for $username: " password
    echo

    # Create the user with a home directory and set the password
    useradd -m -p "$(openssl passwd -1 "$password")" "$username"

    if [ $? -eq 0 ]; then
        echo "User $username created with a home directory and password."
    else
        echo "Failed to create user $username."
        exit 1
    fi
fi
